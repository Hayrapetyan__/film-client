import app from '../plugins/i18n'

export const state = () => ({
  list: []
})

export const mutations = {
  updateList (state, item) {
    let newMovies =  item.slice(0, 10);
    state.list = newMovies
  }
}

export const actions = {
  async getNowPlaying ({ commit }) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/now_playing?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}&page=1&region=US`);
    commit('updateList', response.results)
  }
}

export const getters = {
  get (state) {
    return state.list
  }
}
