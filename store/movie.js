import app from '../plugins/i18n'

export const state = () => ({
  paginationMovies: [],
  movie: {},
  findedMovies: [],
  similarMovies: [],
  reviews: [],
})

export const mutations = {
  updateList (state, item) {
    state.paginationMovies = item
  },
  updateMovieItem (state, item) {
    state.movie = item
  },
  updateSimilars (state, item) {
    state.similarMovies = item
  },
  updateListFinded (state, item) {
    state.findedMovies = item
  },
  updateReviews (state, item) {
    state.reviews = item
  }
}

export const actions = {
  async getMoviesPagination ({ commit }, params) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/popular?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}&page=${params.page}`)
    commit('updateList', response)
  },
  async getSearchedMovies ({ commit }, params) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/search/movie?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}&query=${params.keyword}&page=${params.page}`)
    commit('updateListFinded', response)
  },
  async getMovie ({ commit }, id) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/${id}?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}`);
    commit('updateMovieItem', response)
  },
  async getSimilarMovies ({ commit }, id) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/${id}/similar?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}&page=1`);
    commit('updateSimilars', response.results)
  },
  async getReviews ({ commit }, id) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/${id}/reviews?api_key=${process.env.apiToken}&language=en&page=1`);
    commit('updateReviews', response.results)
  }
}

export const getters = {
  get (state) {
    return state.paginationMovies
  },
  getMovie (state) {
    return state.movie
  },
  getFindedMovies (state) {
    return state.findedMovies
  },
  getSimilars (state) {
    return state.similarMovies
  },
  getReviews (state) {
    return state.reviews
  }
}
