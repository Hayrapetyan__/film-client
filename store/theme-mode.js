export const state = () => ({
  darkMode: JSON.parse(localStorage.getItem('darkMode'))
})

export const mutations = {
  updateThemeMode (state, mode) {
    state.darkMode = mode
    localStorage.setItem('darkMode', mode)
  }
}

export const getters = {
  get (state) {
    return state.darkMode
  }
}
