import app from '../plugins/i18n'

export const state = () => ({
  list: []
})

export const mutations = {
  updateList (state, item) {
    let recomendations = item.slice(0, 10);
    state.list = recomendations
  }
}

export const actions = {
  async getRecommendation ({ commit }, page) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/top_rated?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}&page=${page}`)
    commit('updateList', response.results)
  }
}

export const getters = {
  get (state) {
    return state.list
  }
}
