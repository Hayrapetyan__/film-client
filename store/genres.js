export const state = () => ({
  list: []
})

export const mutations = {
  updateList (state, title) {
    state.list = title
  }
}

export const actions = {
  async getAllGenres ({ commit }) {
      let _this = this
      const response = await _this.$axios.$get(`${process.env.baseUrl}/genre/movie/list?api_key=${process.env.apiToken}&language=ru-RU`);
      commit('updateList', response.genres)
  }
}

export const getters = {
  get (state) {
    return state.list
  }
}
