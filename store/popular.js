import app from '../plugins/i18n'

export const state = () => ({
  list: [],
  trailer: []
})

export const mutations = {
  updateList (state, item) {
    state.list = item
  },
  updateTrailer (state, item) {
    state.trailer = item
  }
}

export const actions = {
  async getPopular ({ commit }) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/popular?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}&page=1`)
    commit('updateList', response.results)
  },

  async getPopularTrailer ({ commit }, id) {
    let _this = this
    const response = await _this.$axios.$get(`${process.env.baseUrl}/movie/${id}/videos?api_key=${process.env.apiToken}&language=${this.app.i18n.locale}`)
    commit('updateTrailer', response.results[0])
  },
}

export const getters = {
  get (state) {
    return state.list
  },
  getTrailer (state) {
    return state.trailer
  }
}
