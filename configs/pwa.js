export default {
  manifest: {
    name: 'iFilm',
    short_name: 'iFilm',
    display: 'standalone',
    background_color: '#fe7900',
    theme_color: '#fe7900',
    description: 'Watch only the best movies'
  },
  workbox: {
    runtimeCaching: [
      { urlPattern: 'https://fonts.googleapis.com/.*' },
      { urlPattern: 'https://fonts.gstatic.com/.*' }
    ]
  }
}
