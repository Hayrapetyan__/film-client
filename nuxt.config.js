import {pwa} from './configs'

export default {
  mode: 'spa',
  server: {
    port: 8000,
    host: '0.0.0.0'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icon.png' }
    ],
    script: [
      { src: '//yohoho.cc/yo.js' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#fe7900',
    height: '4px',
    continuous: true,
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    "~/plugins/v-tooltip.js",
    "~/plugins/i18n.js",
    { src: '~/plugins/vue-carousel.js', mode: 'client' },
    "~/plugins/vue-lazyload.js",
    "~/plugins/vue-star-rating.js"
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/moment',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    ['@nuxtjs/pwa', pwa],
    '@bazzite/nuxt-optimized-images',
    '@neneos/nuxt-animate.css',
    '@nuxtjs/font-awesome',
  ],
  optimizedImages: {
    optimizeImages: true
  },

  /*
    ** environment configs
    */
  env: {
    baseUrl: process.env.apiUrl || 'https://api.themoviedb.org/3',
    apiToken: process.env.token || 'c90960472340983f37679878e271035a',
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {},
  }
}
